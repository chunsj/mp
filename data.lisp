(in-package :mp)

(defun ex/filename (name &key (set :gnuplot-in-action))
  (cond ((eq set :gnuplot-in-action)
         (format nil "~Adata/gnuplot-in-action/datasets/~A"
                 (namestring (asdf:system-source-directory :mp))
                 name))))

(defun ex/df (name &key (set :gnuplot-in-action))
  (let ((fname (ex/filename name :set set)))
    (format nil "'~A'" fname)))

(defun ex/data (name &key (set :gnuplot-in-action))
  (loop :for line :in (slurp (ex/filename name :set set))
        :for cs = (->> (split #\space line)
                       (mapcar #'strim)
                       (remove-if-not (lambda (s) (> ($count s) 0))))
        :collect cs))

(defun ex/output (data)
  (loop :for record :in data
        :do (format T "~&~{~A~^ ~}" record)))

(defun ex/marathon ()
  (let ((lines (ex/data "marathon")))
    (loop :for line :in lines
          :collect (mapcar #'parse/integer line))))
