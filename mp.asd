(defsystem mp
  :name "mp"
  :author "Sungjin Chun <chunsj@gmail.com>"
  :version "24.070"
  :maintainer "Sungjin Chun <chunsj@gmail.com>"
  :license "GPL3"
  :description "my plotting utility for use with emacs and gnuplot"
  :long-description "creates chart files in svg to display inside emacs"
  :depends-on ("mu"
               "uiop")
  :components ((:file "mp")
               (:file "data")))
