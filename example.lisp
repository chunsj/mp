(defpackage :mp.examples
  (:use #:common-lisp
        #:mu
        #:mp))

(in-package :mp.examples)

;; XXX
;; I should be more open to the dataframe.
;; Dataframe could be built from data files.
;; Build the dataframe, then make operations on it.
;; XXX

;; gnuplot in action examples

(with-plot ()
  (cmd "set samples 1000")
  (cmd "plot sin(x)"))

(with-plot ()
  (cmd "plot ~A u 1:2 w boxes" (ex/df "marathon")))

(with-plot ()
  (cmd "plot '-' u 1:2 w boxes")
  (ex/output (ex/marathon)))

(with-plot ()
  (cmd "plot ~A u 1:2 w lines" (ex/df "runtime")))

(with-plot ()
  (cmd "set xlabel 'Cluster Size'")
  (cmd "set ylabel 'Run Time [sec]'")
  (cmd "set logscale")
  (cmd "set key")
  (cmd "set xrange [1000:]")
  (cmd "set yrange [1:]")
  (cmd "data=~A" (ex/df "runtime"))
  (plt "data u 1:2 title 'Data' w lines"
       "(x/2500)**3 title 'Model'"))

(with-plot ()
  (cmd "unset border")
  (cmd "unset xtics")
  (cmd "unset ytics")
  (cmd "set size square")
  (cmd "data=~A" (ex/df "cluster"))
  (plt "data u 1:2 title 'Cluster' w dots"))

(with-plot ()
  (cmd "set samples 1000")
  (plt "sin(x)"
       "x"
       "x-(x**3)/6"))

(with-plot ()
  (cmd "set samples 1000")
  (plt "[][-2:2] sin(x)"
       "x"
       "x-(x**3)/6"))

(with-plot ()
  (cmd "data=~A" (ex/df "prices"))
  (plt "data u 1:2"
       "data u 1:3"))

(with-plot ()
  (cmd "data=~A" (ex/df "prices"))
  (plt "data u 1:2 title 'PQR' w lines"
       "data u 1:3 title 'XYZ' w linespoints"))

(with-plot ()
  (plt (format nil "~A u 2:3 w points" (ex/df "prices"))))

(with-plot ()
  (plt (format nil "[:12000] ~A u 1:2 title 'Men'" (ex/df "records"))
       (format nil "~A u 1:3 title 'Women'" (ex/df ""))
       "0.14*x"
       "0.16*x"))

(with-plot ()
  (cmd "cmd(x) = exp(-0.5*(x/1.0)**2)/1.0")
  (plt "cmd(x)"))

(with-plot ()
  (cmd "cmd(x,s) = exp(-0.5*(x/s)**2)/s")
  (plt "cmd(x,1)"
       "cmd(x,2)"
       "cmd(x,3)"))

(with-plot ()
  (cmd "set logscale")
  (plt (format nil "[9:][5:11] ~A u 2:($1/$2) title 'Men' w linespoints"
               (ex/df "records"))
       "'' u 3:($1/$3) title 'Women' w linespoints"))

(with-plot ()
  (plt (format nil "~A u 1:(-0.1) w points pointtype 7"
               (ex/df "measurements"))
       "'' u 1:(1/12.0) smooth kdens bandwidth 0.1"
       "'' u 1:(1/12.0) smooth kdens bandwidth 0.2"
       "'' u 1:(1/12.0) smooth cumul"))

(with-plot ()
  (cmd "unset key")
  (cmd "set multiplot layout 2,2 margins 0.06,0.95,0.085,0.95 spacing 0.075,0.1")
  (cmd "set xtics 2,2")
  (cmd "set mxtics 2")
  (cmd "set label 1 'Orders per Day' at 8,3")
  (cmd "data=~A" (ex/df "orders"))
  (plt "[1:14] data u (strcol(2)[9:10]+0):(1) s f w lp pt 7")
  (cmd "set label 1 \"Histogram:\\nItems per Order\" at 3.5,50")
  (plt "[0:7] '' u 4:(1) s f w boxes")
  (cmd "set label 1 'Value vs Weight' at 4,7")
  (plt "'' u 5:6 w p pt 6")
  (cmd "set xtics ('Date' 1, 'Items' 2, 'Weight' 3, 'Value' 4, 'Ship' 5)")
  (cmd "unset ytics")
  (cmd "set style parallel lw 1")
  (cmd "set linetype 1000 lc '0xff000000'")
  (cmd "ship(c) = 0+strcol(c)[1:1]")
  (cmd "unset label 1")
  (plt "[1:5] '' u (strcol(2)[9:10]+0):4:5:6:(ship(7)) w parallel lt 10"
       "'' u (strcol(2)[9:10]+0):4:5:6:(ship(7)):(ship(7)==0?1:1000) w parallel lc var")
  (cmd "unset multi"))

(with-plot ()
  (cmd "data=~A" (ex/df "traffic1"))
  (plt "data index 1 using 1:2 w linespoints"))

(with-plot ()
  (cmd "set key")
  (cmd "data=~A" (ex/df "sequence"))
  (plt "[0:4][-0.25:1.5] data u 1:2 w lp pt 6 ps 0.5"
       "'' u 1:3 w lp pt 7 ps 0.5"
       "'' u 1:4 w lp pt 8 ps 0.5"
       "-log(x)+(x-1) w l"))

(with-plot ()
  (cmd "set xlabel 'Cluster size [thousands]'")
  (cmd "set ylabel 'Running time [seconds]'")
  (cmd "set title 'DLS Growth Program: Runtime vs Cluster Size (Double Logarithmic)'")
  (cmd "set logscale")
  (cmd "set key top left")
  (cmd "data=~A" (ex/df "progress"))
  (plt "[1:100][0.1:] data u 1:2 title 'Data' w p ps 0.5"
       "(x/2.5)**3 notitle"))

(with-plot ()
  (cmd "set style data linesp")
  (cmd "set key autotitle columnhead")
  (cmd "data=~A" (ex/df "grains"))
  (plt "data u 1:2 ps 0.5"
       "'' u 1:3 ps 0.5"
       "'' u 1:4 ps 0.5"))

(with-plot ()
  (cmd "set style data linesp")
  (cmd "data=~A" (ex/df "grains"))
  (plt "data u 1:2 title 'Wheat' at beginning ps 0.5"
       "'' u 1:3 title columnhead at end ps 0.5"
       "'' u 1:4 title at beginning ps 0.5"))

(with-plot ()
  (cmd "set title 'Scattering: Data and Theory'")
  (cmd "set xlabel 'Frequency Offset'")
  (cmd "set ylabel 'Relative Intensity'")
  (cmd "set key top left box title 'Legend' height 0.5")
  (cmd "set arrow 1 from -4,0.35 to -3.4,0.2 filled size 0.2,15")
  (cmd "set label 1 'Cusp at x=-π?' at -6,0.4")
  (cmd "set arrow 2 from 5,-0.2 to 5,0.95 nohead dashtype '-' lc black")
  (cmd "set label 2 'Singularity at x=5' at 5,1")
  (cmd "set arrow 3 from -1,0.2 to 1,0.2 heads size 0.1,90")
  (cmd "set label 3 \"Full Width at\\nHalf Maximum\" at 0,0.125 center")
  (cmd "data=~A" (ex/df "spectrum"))
  (plt "[-7:7][-0.2:1.1] data u 1:2 t 'Data' w p pt 7 ps 0.5"
       "1/(1+x**2) t 'Lorentzian'"
       "exp(-x**2) t 'Gausian'"))

(with-plot ()
  (cmd "set xdata time")
  (cmd "set timefmt '%Y-%m-%d'")
  (cmd "set xtics format '%d %b'")
  (cmd "set ytics nomirror")
  (cmd "set y2tics")
  (cmd "set ylabel 'Mean Icecream Consumption per Head [grams]'")
  (cmd "set y2label 'Mean Temperature [celcius]'")
  (cmd "set key top left reverse left")
  (cmd "set pointsize 0.5")
  (cmd "data=~A" (ex/df "icecream"))
  (plt "['1951-03-25':] data u 1:2 t 'Icecream' axes x1y1 w lp"
       "'' u 1:3 axes x1y2 t 'Temperature' w lp"))

(with-plot ()
  (cmd "unset border")
  (cmd "set key")
  (cmd "set samples 1000")
  (cmd "set zeroaxis lt -1")
  (cmd "set xtics axis")
  (cmd "set ytics axis")
  (plt "[-8:4] airy(x)"))

(with-plot ()
  (cmd "data=~A" (ex/df "months"))
  (plt "[][0:10] data u 1:2:xtic(3) w linesp"))

(with-plot ()
  (cmd "data=~A" (ex/df "alpha"))
  (plt "[-0.4:1.2][-0.2:1.2] data u 2:1 w p pt 7 ps 1 lc rgb '#cc0000ff'"
       "'' u 2:1 w p pt 6 ps 1 lc rgb '#ddff0000'"))

(defun glass.def ()
  (cmd "set datafile separator ','")
  (cmd "data=~A" (ex/df "glass.data"))
  (cmd "names='ID RfIx Na Mg Al Si K Ca Ba Fe Type'")
  (cmd "types='Bldg1 Bldg2 Vhcl1 Vhcl2 Cont Tbl Lamp'"))

(with-plot ()
  (glass.def)
  (cmd "unset xtics")
  (cmd "unset key")
  (cmd "set ticscale 0")
  (cmd "set grid lt 1 lc rgb 'grey' lw 2")
  (cmd "set ytic offset 0.5,0")
  (cmd "set style fill solid 0.5")
  (cmd "set multiplot layout 1,9 margin 0.075,0.975,0.1,0.95 spacing 0.075,0")
  (cmd "do for[k=2:10] {")
  (cmd "  set xlabel word(names,k)")
  (cmd "  plot data u (1):k:(0.5) w boxplot pt 6")
  (cmd "}")
  (cmd "unset multiplot"))

(with-plot ()
  (glass.def)
  (cmd "unset xtics")
  (cmd "unset key")
  (cmd "set multiplot layout 3,3 margin 0.075,0.99,0.015,0.975 spacing 0.055,0.035")
  (cmd "do for[k=2:10] {")
  (cmd "  set label word(names,k) at graph 0.95,0.9 right")
  (cmd "  plot data u (1):k:(0.5):11 w boxplot pt 6")
  (cmd "  unset label")
  (cmd "}")
  (cmd "unset multiplot"))

(with-plot ()
  (glass.def)
  (cmd "col=6")
  (cmd "get(s,k)=word(s,int(k))")
  (cmd "unset key")
  (cmd "set title 'Attribute: ' . get(names,col)")
  (cmd "s = ''")
  (cmd "do for[k=1:7] {")
  (cmd "  stats data u ($11==k?1:0) noout")
  (cmd "  s = s . sprintf(\" %d \", STATS_sum)")
  (cmd "}")
  (plt "data u (1):col:(0.01*get(s,$11)):(get(types,$11)) w boxplot"))

(with-plot ()
  (glass.def)
  (cmd "unset key")
  (cmd "unset tics")
  (cmd "set size square")
  (cmd "set multiplot layout 9,9 margin 0.01,0.99,0.01,0.99 spacing 0.01")
  (cmd "nn=10")
  (cmd "do for [i=2:nn] {")
  (cmd "  do for [j=2:nn] {")
  (cmd "    if(i!=j) {")
  (cmd "      if(i<j) {")
  (cmd "        plot data u i:j w p ps 0.36 pt 7 lc rgb '#e6ff0f00'")
  (cmd "      } else {")
  (cmd "        plot data u i:j w p ps 0.36 pt 7 lc rgb '#e6000fff'")
  (cmd "      }")
  (cmd "    } else {")
  (cmd "      unset border")
  (cmd "      set label word(names,i) at 0,0 center")
  (cmd "      plot [-1:1][-1:1] 1/0")
  (cmd "      unset label")
  (cmd "      set border")
  (cmd "    }")
  (cmd "  }")
  (cmd "}")
  (cmd "unset multiplot"))

;; cookbook examples

(defun datafile (ch name)
  (format nil "../scratch/gnuplot-cookbook/7249_~A_Code/datafiles/~A" ch name))

(defun ex/data (ch name) (slurp (datafile ch name)))

(defun ex/df (ch fname)
  (if (> ($count fname) 0)
      (format nil "'~A'" (datafile ch fname))
      (format nil "''")))

(with-plot ()
  (plt "besj0(x)"))

(with-plot ()
  (plt "[-5:5] (sin(1/x) - cos(x))*erfc(x)"))

(ex/data "01" "parabola.text")

(with-plot ()
  (cmd "data=~A" (ex/df "01" "parabola.text"))
  (plt "[-1:1] data"
       "-x"
       "-x**3"))

(with-plot ()
  (cmd "set y2tics -100,10")
  (cmd "set ytics nomirror")
  (plt "sin(1/x) axis x1y1"
       "100*cos(x) axis x1y2"))

(with-plot ()
  (cmd "set y2tics -100,10")
  (cmd "set ytics nomirror")
  (cmd "set x2tics -20,2")
  (cmd "set xtics nomirror")
  (cmd "set xrange [-10:10]")
  (cmd "set x2range [-20:0]")
  (plt "sin(1/x) axis x1y1"
       "100*cos(x-1) axis x2y2"))

(with-plot ()
  (cmd "data=~A" (ex/df "01" "scatter.dat"))
  (plt "data with points pt 7"))

(with-plot ()
  (cmd "set style fill pattern")
  (cmd "set samples 100")
  (plt "[-6:6] besj0(x) with boxes"
       "sin(x) with boxes"))

(with-plot ()
  (cmd "set key off")
  (cmd "data=~A" (ex/df "01" "parabolaCircles.text"))
  (plt "data with circles"))

(with-plot ()
  (cmd "set samples 100")
  (plt "[0:50] besj0(x) with filledcurves above y1=0.07"))

(with-plot ()
  (cmd "set bars 2")
  (cmd "data=~A" (ex/df "01" "finance.dat"))
  (plt "[0:100] data using 0:2:3:4:5 notitle with financebars"))

(with-plot ()
  (cmd "data=~A" (ex/df "01" "parabola.text"))
  (plt "[-2:2] data with histeps"))

(with-plot ()
  (cmd "data=~A" (ex/df "01" "parabolaCircles.text"))
  (cmd "set style fill solid 1.0 border lt -1")
  (cmd "set style data histograms")
  (cmd "set style histogram rowstacked")
  (plt "[0:40] data using (-$2)"
       "'' using (20*$3) notitle"))

(with-plot ()
  (cmd "data=~A" (ex/df "01" "parabolaCircles.text"))
  (cmd "set style fill solid 1.0")
  (cmd "set style data histograms")
  (plt "[0:40] data using (-$2)"
       "'' using (20*$3) notitle"))

(with-plot ()
  (cmd "data=~A" (ex/df "01" "parabolaCircles.text"))
  (cmd "set pointsize 2")
  (cmd "set bars 3")
  (plt "[1:3] data using 1:(-$2):3 with errorbars"
       "'' using 1:(-$2):3 pt 7 notitle"))

(with-plot ()
  (cmd "data=~A" (ex/df "01" "parabolaCircles.text"))
  (cmd "set pointsize 2")
  (cmd "set bars 3")
  (cmd "set style fill pattern 2 border lt -1")
  (plt "[1:3] data using 1:(-$2):3 with boxerrorbars"))

(with-plot (T)
  (cmd "data=~A" (ex/df "01" "candlesticks.dat"))
  (cmd "set xrange [0:11]")
  (cmd "set yrange [0:10]")
  (cmd "set boxwidth 0.2")
  (plt "data using 1:3:2:6:5 with candlesticks lt -1 lw 2 whiskerbars"
       "'' using 1:4:4:4:4 with candlesticks lt -1 lw 2 notitle"))

(with-plot ()
  (cmd "set samples 60")
  (plt "[0:2*pi] sin(x) with impulses lw 2"))

(with-plot ()
  (cmd "set samples 60")
  (plt "[0:2*pi] exp(-x/4.0)*sin(x) with impulses lw 2 notitle"
       "exp(-x/4.0)*sin(x) with points pt 7"))

(with-plot ()
  (cmd "set parametric")
  (plt "sin(7*t), cos(11*t) notitle"))

(with-plot ()
  (cmd "set parametric")
  (cmd "set trange [0:2]")
  (plt "sin(7*t), cos(11*t) notitle"))

(with-plot ()
  (cmd "set xtics axis nomirror")
  (cmd "set ytics axis nomirror")
  (cmd "set zeroaxis")
  (cmd "unset border")
  (cmd "set polar")
  (plt "[0:12*pi] t"))

(ex/data "02" "ch2.dat")

(with-plot ()
  (cmd "data=~A" (ex/df "02" "ch2.dat"))
  (cmd "set yrange [-1.5:1.5]")
  (cmd "set xrange [0:6.3]")
  (cmd "set ytics nomirror")
  (cmd "set y2tics 0,0.1")
  (cmd "set y2range [0:1.2]")
  (cmd "set style fill pattern 5")
  (cmd "set xlabel 'Time (sec.)'")
  (cmd "set ylabel 'Amplitude'")
  (cmd "set y2label 'Error Magnitude'")
  (cmd "set title 'Fourier Approximation to Square Wave'")
  (plt "data using 1:2:(sgn($2)) with filledcurves"
       "'' using 1:2 with lines lw 2 notitle"
       "'' using 1:(sgn($2)) with lines notitle"
       "'' using 1:(abs(sgn($2)-$2)) with lines axis x1y2"))

(with-plot ()
  (cmd "data=~A" (ex/df "02" "ch2.dat"))
  (cmd "set yrange [-1.5:1.5]")
  (cmd "set xrange [0:6.3]")
  (cmd "set ytics nomirror")
  (cmd "set y2tics 0,0.1")
  (cmd "set y2range [0:1.2]")
  (cmd "set style fill pattern 5")
  (cmd "set key at graph 0.9,0.9 spacing 3")
  (cmd "set xlabel 'Time (sec.)'")
  (cmd "set ylabel 'Amplitude'")
  (cmd "set y2label 'Error Magnitude'")
  (cmd "set title 'Fourier Approximation to Square Wave'")
  (plt "data using 1:2:(sgn($2)) with filledcurves notitle"
       "'' using 1:2 with lines lw 2 title 'Square Wave'"
       "'' using 1:(sgn($2)) with lines title 'Fourier Approximation'"
       "'' using 1:(abs(sgn($2)-$2)) with lines axis x1y2 title 'Error Magnitude'"))

(with-plot ()
  (cmd "set key outside")
  (plt "sin(x) title 'sine'"
       "cos(x) title 'cosine'"))

(with-plot ()
  (cmd "set key outside")
  (cmd "set key box lt -1 lw 1")
  (plt "sin(x) title 'sine'"
       "cos(x) title 'cosine'"))

(with-plot ()
  (cmd "data=~A" (ex/df "02" "ch2.dat"))
  (cmd "set yrange [-1.5:1.5]")
  (cmd "set xrange [0:6.3]")
  (cmd "set ytics nomirror")
  (cmd "set y2tics 0,0.1")
  (cmd "set y2range [0:1.2]")
  (cmd "set style fill pattern 5")
  (cmd "set key at graph 0.9,0.9 spacing 3")
  (cmd "set xlabel 'Time (sec.)'")
  (cmd "set ylabel 'Amplitude'")
  (cmd "set y2label 'Error Magnitude'")
  (cmd "set title 'Fourier Approximation to Square Wave'")
  (cmd "set label '근사로 인한 오류' right at 2.4,0.45 offset -0.5,0")
  (cmd "set arrow 1 from first 2.4,0.45 to 3,0.93 lt 1 lw 2 front size 0.3,15")
  (plt "data using 1:2:(sgn($2)) with filledcurves notitle"
       "'' using 1:2 with lines lw 2 title 'Square Wave'"
       "'' using 1:(sgn($2)) with lines title 'Fourier Approximation'"
       "'' using 1:(abs(sgn($2)-$2)) with lines axis x1y2 title 'Error Magnitude'"))

(with-plot ()
  (cmd "set border lw 0.25")
  (cmd "set key top left")
  (plt "[0:1] x**0.5 lc rgb 'orange' lw 4"
       "x lc rgb 'steelblue' lw 4"
       "x**2 lc rgb 'bisque' lw 4"
       "x**3 lc rgb 'seagreen' lw 4"))

(with-plot ()
  (cmd "set border lw 0.25")
  (cmd "set key top left")
  (plt "[0:1] x**0.5 lc rgb 'orange' lw 4"
       "x lc rgb 'steelblue' lw 4"
       "x**2 lc rgb 'bisque' lw 4"
       "x**3 lc rgb 'seagreen' lw 4"))

(with-plot ()
  (cmd "set grid")
  (cmd "set mxtics 4")
  (cmd "set mytics 2")
  (plt "[0:2*pi] sin(12*x)*exp(-x/4)"))

(with-plot ()
  (cmd "set ytics nomirror")
  (cmd "set y2tics 0.4")
  (cmd "set my2tics 4")
  (cmd "set xtics pi/4.0")
  (cmd "set mxtics 4")
  (cmd "set grid")
  (plt "[0:2*pi] sin(x) axis x1y1, 2*cos(8*x)*exp(-x) axis x1y2"))

(with-plot ()
  (cmd "set tics scale 3")
  (cmd "set mxtics 4")
  (cmd "set mytics 4")
  (plt "[0:4*pi] sin(x)/x"))

(with-plot ()
  (cmd "unset tics")
  (plt "[0:4*pi] sin(x)/x"))

(with-plot ()
  (cmd "set xtics axis nomirror")
  (cmd "set ytics axis nomirror")
  (cmd "set zeroaxis")
  (cmd "unset border")
  (cmd "set polar")
  (cmd "set samples 500")
  (cmd "set grid")
  (cmd "set xtics 15,2,30")
  (cmd "set ytics 10,2,20")
  (plt "[0:12*pi] t"))

(with-plot ()
  (cmd "set tics out")
  (cmd "set grid")
  (plt "[-3:3] tanh(x)"))

(with-plot ()
  (cmd "set xtics ('pi' pi, 'pi/2' pi/2, '2pi' 2*pi, '3pi/2' 3*pi/2, '0' 0)")
  (plt "[0:2*pi] sin(x)"))

(with-plot ()
  (with-multiplot ("layout 2,2")
    (plt "besj0(x)")
    (plt "besj1(x)")
    (plt "besy0(x)")
    (plt "besy1(x)")))

(with-plot ()
  (with-multiplot ()
    (cmd "set object ellipse center 0.13,0 size 0.4,4")
    (cmd "set arrow from 0.1,2.1 to screen 0.22,0.4 front lt 3")
    (cmd "set samples 1000")
    (cmd "set grid")
    (cmd "set xtics 0.4")
    (cmd "set ytics 4")
    (plt "[0:2*pi] exp(x)*sin(1/x)")
    (cmd "set origin 0.2,0.4")
    (cmd "set size 0.5,0.5")
    (cmd "clear")
    (cmd "unset key")
    (cmd "unset grid")
    (cmd "unset object")
    (cmd "unset arrow")
    (cmd "set xtics 0.1")
    (cmd "set ytics 0.5")
    (cmd "set bmargin 1")
    (cmd "set tmargin 1")
    (cmd "set lmargin 3")
    (cmd "set rmargin 1")
    (plt "[0:0.2] exp(x)*sin(1/x)")))

(with-plot ()
  (cmd "set xrange [-pi:pi]")
  (cmd "unset key")
  (with-multiplot ("layout 2,2 title 'Derivatives of sin(x)' font 'Times Roman,12'")
    (cmd "set style arrow 1 head filled size screen 0.03,15,135 lt 2 lw 2")
    (cmd "set arrow 1 from screen 0.45,0.84 to screen 0.65,0.84 arrowstyle 1")
    (cmd "set arrow 2 from screen 0.87,0.64 to screen 0.87,0.3 arrowstyle 1")
    (cmd "set arrow 3 from screen 0.7,0.15 to screen 0.4,0.15 arrowstyle 1")
    (cmd "set arrow 4 from screen 0.35,0.35 to screen 0.35,0.7 arrowstyle 1")
    (cmd "set title 'sin(x)'")
    (plt "sin(x)")
    (cmd "set title \"sin\\'(x) = cos(x)\"")
    (plt "cos(x)")
    (cmd "set title \"sin\\'\\'\\'(x) = cos\\'\\'(x) = -sin\\'(x) = -cos(x)\"")
    (plt "-cos(x)")
    (cmd "set title \"sin\\'\\'(x) = cos\\'(x) = -sin(x)\"")
    (plt "-sin(x)")))

(with-plot ()
  (cmd "set samples 2000")
  (cmd "f(x) = x < 0 ? sin(x) : NaN")
  (cmd "g(x) = x >= 0 ? exp(-x/5.0)*sin(x) : NaN")
  (plt "[-20:20] f(x),g(x)"))

(with-plot ()
  (cmd "data=~A" (ex/df "07" "rs.dat"))
  (plt "data with lines lw 0.5 notitle"
       "'' smooth bezier lw 2 title 'Bezier Smoothed'"))

(with-plot ()
  (cmd "data=~A" (ex/df "07" "rs.dat"))
  (cmd "f(x) = a*sin(b*x)")
  (cmd "fit f(x) data via a,b")
  (plt "data with lines lw 0.5 notitle"
       "f(x) lw 2 title 'Fit by GNUPLOT'"))

(with-plot ()
  (cmd "data=~A" (ex/df "07" "randomnormal.text"))
  (plt "data using 1:(0.001) smooth kdensity title 'Frequency Distribution'"))

(with-plot ()
  (cmd "data=~A" (ex/df "07" "randomnormal.text"))
  (cmd "set key top left")
  (plt "data using 1:(0.001) smooth cumul title 'Cumulative Distribution'"))

(with-plot ()
  (cmd "set isosamples 40")
  (cmd "unset key")
  (cmd "set title 'J_0(r^2)'")
  (cmd "set xrange [-4:4]")
  (cmd "set yrange [-4:4]")
  (cmd "set ztics 1")
  (spl "besj0(x**2 + y**2)"))

(with-plot ()
  (cmd "set isosamples 40")
  (cmd "unset key")
  (cmd "set title 'J_0(r^2)'")
  (cmd "set xrange [-4:4]")
  (cmd "set yrange [-4:4]")
  (cmd "set ztics 1")
  (cmd "set hidden3d")
  (spl "besj0(x**2 + y**2)"))

(with-plot ()
  (cmd "set mapping cylindrical")
  (cmd "unset tics")
  (cmd "unset border")
  (cmd "set hidden")
  (cmd "unset key")
  (cmd "set isosamples 40")
  (cmd "set xrange [-pi:pi]")
  (cmd "set yrange [-pi:pi]")
  (cmd "set zrange [0:pi]")
  (cmd "set iso 60")
  (spl "'++' using 1:2:(sin($2)) with lines"))

(with-plot ()
  (cmd "set isosamples 100")
  (cmd "set samples 100")
  (cmd "unset key")
  (cmd "set title 'J_0(r^2)'")
  (cmd "set xrange [-4:4]")
  (cmd "set yrange [-4:4]")
  (cmd "set ztics 1")
  (cmd "unset surface")
  (cmd "set pm3d")
  (spl "besj0(x**2 + y**2)"))
