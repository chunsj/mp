(defpackage :mp
  (:use #:common-lisp
        #:mu)
  (:export #:cmd
           #:plt
           #:spl
           #:with-multiplot
           #:with-input-data
           #:with-plot
           #:ex/filename
           #:ex/data
           #:ex/df
           #:ex/output
           #:ex/marathon)
  (:import-from #:uiop
                #:run-program))

(in-package :mp)

(defvar *plt-width* 630)
(defvar *plt-height* 500)

(defvar *plt-output* "/tmp/emacs-chart.svg")

;; refer colors
;; https://spectrum.adobe.com/page/color-for-data-visualization/
(defvar *plt-colors* (let ((rgbs '((0 192 199)
                                   (81 68 211)
                                   (232 135 26)
                                   (218 52 144)
                                   (144 137 250)
                                   (71 226 111)
                                   (39 128 235)
                                   (111 56 177)
                                   (223 191 3)
                                   (203 111 16)
                                   (38 141 108)
                                   (155 236 84))))
                       (loop :for rgb :in rgbs
                             :collect (format nil "#~{~A~}"
                                              (mapcar (lambda (v) (format nil "~2,'0X" v)) rgb)))))

;; (defvar *plt-colors* '("#f92672" "#dd98f2" "#f68678" "#d7a575" "#92d690" "#8293ef"
;;                        "#66d9ef" "#40e0d0" "#ae81ff"))

(defvar *gnuplot* "gnuplot")

(defun load-config ()
  (let ((fn (strcat (namestring (user-homedir-pathname)) ".mp.lsp")))
    (when (probe-file fn)
      (let ((cfg (slurp/lsp fn)))
        (let ((size ($ cfg :size)))
          (when ($ size :width)
            (setf *plt-width* ($ size :width)))
          (when ($ size :height)
            (setf *plt-height* ($ size :height))))
        T))))

(defun gp (str)
  (format T str)
  (format T "~%"))

(defun gp* (fmt &rest args) (gp (apply #'format nil fmt args)))

(defun cmd (fmt &rest args) (apply #'gp* fmt args))

(defun plt (&rest plot-commands)
  (gp* "plot ~{~A~^, ~}" plot-commands))

(defun spl (&rest plot-commands)
  (gp* "splot ~{~A~^, ~}" plot-commands))

(defun setup-terminal (width height)
  (let ((fmt "set terminal svg size ~A,~A enhanced background rgb \"white\" font \"Avenir,10\""))
    (gp (format nil fmt width height))))

(defun setup-output (output)
  (gp (format nil "set output \"~A\"" output)))

(defun setup-base ()
  (gp "set tics font \"Avenir,8\" scale 0.5")
  (gp "set key font \"Avenir,8\"")
  (gp "set title font \"Avenir Bold,11\"")
  (gp "set grid back lt 1 lw 0.1 lc rgb \"#909090\"")
  (gp "set pointsize 0.64")
  (gp "set samples 1000")
  (gp "set encoding utf8")
  (loop :for color :in *plt-colors*
        :for i :from 1
        :do (gp (format nil "set linetype ~A lc rgb \"~A\"" i color)))
  (gp (format nil "set palette maxcolors ~A" ($count *plt-colors*)))
  (gp (format nil "set palette defined ( ~{~A~^, ~} )"
              (loop :for i :from 0
                    :for color :in *plt-colors*
                    :collect (format nil "~A \"~A\"" i color)))))

(defun setup-end () (gp "~%set output"))

(defmacro with-input-data ((&optional (name "$mpds")) &body body)
  `(progn
     (gp (format nil "~A << ENDOFMPDATASET" ,name))
     ,@body
     (gp "")
     (gp "ENDOFMPDATASET")))

(defmacro with-multiplot ((&optional arg) &body body)
  `(progn
     (if ,arg
         (gp (format nil "set multiplot ~A" ,arg))
         (gp "set multiplot"))
     ,@body
     (gp "unset multiplot")))

(defun make-script (function)
  (load-config)
  (let ((*standard-output* (make-string-output-stream)))
    (setup-terminal *plt-width* *plt-height*)
    (setup-output *plt-output*)
    (setup-base)
    (funcall function)
    (setup-end)
    (terpri)
    (get-output-stream-string *standard-output*)))

(defmacro with-plot ((&optional debug) &body body)
  `(unwind-protect
        (with-input-from-string (in (let ((script (make-script (lambda () ,@body))))
                                      (when ,debug (prn script))
                                      script))
          (multiple-value-bind (r1 r2 r3)
              (run-program *gnuplot*
                           :input in
                           :output :interactive
                           :error-output :interactive)
            (declare (ignore r1 r2))
            r3))))
